var TileGame = function(){

  let $modal = document.getElementById('modal');
  let $levels = document.getElementsByClassName('level')[0];
  let $gameLevel = document.getElementById('game-level');
  let $tiles = document.getElementsByClassName('tiles');
  let $timer = document.getElementById('timer');
  let $mainTiles = document.getElementById('main-tiles');
  let $score = document.getElementById('current-score');
  let $highScore = document.getElementById('high-score');
  let $restart = document.getElementById('restart');

  let gameMode = '';
  let time = 120;
  let highScore = Number(localStorage.getItem('highScore'));
  let timer = ''

  let setGameMode = function(gameLevel){
    gameMode = gameLevel;
    $modal.style.display = 'none';
    $gameLevel.innerHTML= gameLevel;
    $gameLevel.style['font-weight'] = 600;
    initalizeTiles();
  }

  let initalizeTiles = function(){
    let n = 3;
    if(gameMode === "Medium"){
      n = 4;
    }else if (gameMode === "Hard") {
      n = 6;
    }

    for(let ii = 0; ii < n; ii++){
      let $tile = $tiles[ii];
      for(let jj = 0; jj < n; jj ++){
        let div = document.createElement('div');
        div.classList.add('tile');
        $tile.appendChild(div);
      }
    }

    setTime();
  }

  let setTime = function(){
    let prevRand = 0;
    let tiles = document.getElementsByClassName('tile');

    timer = setInterval(()=>{
      time--;
      $timer.innerHTML = time;
      if(time == 0){
        let latestScore = Number($score.innerHTML)
        if(latestScore > highScore){
          localStorage.setItem('highScore', latestScore);
        }
        clearInterval(timer);
        alert('Game Over');
        restartGame();
      }

      tiles[prevRand].classList.remove('glow');
      let tileLength = tiles.length;
      let randomNum = Math.floor((Math.random() * tileLength));
      prevRand = randomNum;
      let chosenTile = tiles[randomNum];
      chosenTile.classList.add('glow');

    }, 1000);
  }

  let restartGame = function(){
    time = 120;
    clearInterval(timer);
    $timer.innerHTML = time;
    $score.innerHTML = 0;
    for(let ii = 0; ii < 6; ii++){
      $tiles[ii].innerHTML = '';
    }

    $highScore.innerHTML = highScore;
    $modal.style.display = 'block';
  }

  let bindEvents = function(){
    $levels.addEventListener('click', (e)=>{
      let val = e.target.innerHTML;
      if(val === 'Easy'){
        setGameMode('Easy');
      }else if (val === 'Medium') {
        setGameMode('Medium');
      }else if (val === 'Hard') {
        setGameMode('Hard');
      }
    }, false);


    $mainTiles.addEventListener('click', (e)=>{
      let currentScore = Number($score.innerHTML)
      if(/glow/.test(e.target.classList.value)){
         currentScore += 1;
      }else {
        currentScore -= 1;
      }
      $score.innerHTML = currentScore;
    }, false);

    $restart.addEventListener('click', ()=>{
      restartGame()
    }, false);
  }

  let initalizeGame = function(){
    bindEvents();
    $highScore.innerHTML = highScore;
    $modal.style.display = 'block';
  }

  return {
    initalizeGame: initalizeGame
  }
}()

TileGame.initalizeGame();
